# setup

## Debian

### Setup sudo

Add user to the sudo group: 

`adduser bartek sudo`

### Run setup script

`sh -c "$(wget https://gitlab.com/bartosz.gadzala/machine-setup/raw/master/setup.sh -O -)"`