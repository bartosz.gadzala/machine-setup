#!/bin/sh

DEVELOPER=$(whoami)

echo "Setting up dev machine for ${DEVELOPER}..."

# ------------------------------------------------------------------------------
# Debian prerequisites
# ------------------------------------------------------------------------------
echo "Updating apt cache..."
sudo apt update

echo "Upgrading all existing packages..."
sudo apt upgrade -y

echo "Installing essential packages..."
sudo apt install -y vim wget curl tig tmux zsh \
     software-properties-common apt-transport-https snapd \
     chromium youtube-dl openjdk-8-jdk ca-certificates gnupg2 \
     build-essential rsync tree psmisc ntp pm-utils \
     i3 i3lock dmenu acpi lm-sensors

echo "Setting up NTP..."
sudo systemctl enable ntp
sudo systemctl restart ntp

echo "Installing Docker CE..."
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"
sudo apt update
sudo apt install -y docker-ce docker-ce-cli containerd.io
sudo adduser ${DEVELOPER} docker

echo "Installing Visual Studio Code..."
curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
sudo install -o root -g root -m 644 microsoft.gpg /etc/apt/trusted.gpg.d/
sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
sudo apt-get update
sudo apt-get install code

echo "Installing Intellij Idea CE..."
sudo snap install intellij-idea-community --classic

echo "Installing Android Studio..."
sudo snap install android-studio --classic

echo "Installing Spotify..."
sudo snap install spotify

echo "Installing Slack..."
sudo snap install slack --classic

# ------------------------------------------------------------------------------
# General prerequisites
# ------------------------------------------------------------------------------

echo "Installing ohmyz.sh..."
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
if [ ! -d ${HOME}/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting ]; then
    echo "Installing zsh syntax highlighting..."
    git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ~/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting
fi

# ------------------------------------------------------------------------------
# Dotfiles
# ------------------------------------------------------------------------------
config() {
    /usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME $@
}

echo "Downloading latest dotfiles..."
if [ ! -d ${HOME}/.cfg ]; then
    echo ".cfg" >> .gitignore
    git clone --bare https://gitlab.com/bartosz.gadzala/dotfiles.git $HOME/.cfg
    mkdir -p .config-backup
    config checkout
    if [ $? = 0 ]; then
        echo "Checked out config."
    else
        echo "Backing up pre-existing dot files.";
        config checkout 2>&1 | egrep "\s+\." | awk {'print $1'} | xargs -I{} mv {} .config-backup/{}
    fi;
    config checkout
    config config --local status.showUntrackedFiles no
else
    config pull
fi
